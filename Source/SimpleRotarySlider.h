/*
  ==============================================================================

    SimpleRotarySlider.h
    Created: 11 Oct 2021 10:08:24pm
    Author:  ihorv

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

//==============================================================================
/*
*/
class SimpleRotarySlider  : public juce::Slider
{
public:
    SimpleRotarySlider();
    SimpleRotarySlider(juce::RangedAudioParameter* rap);
    ~SimpleRotarySlider() override;

    void paint (juce::Graphics&) override;
    juce::Rectangle<int> getSliderBounds() const;
    juce::String getDisplayString() const;
    void resized() override;

private:
    juce::RangedAudioParameter* m_param;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (SimpleRotarySlider)
};
