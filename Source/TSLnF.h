/*
  ==============================================================================

    TSLnF.h
    Created: 10 Oct 2021 5:13:15pm
    Author:  ihorv

  ==============================================================================
*/

#pragma once
#include <JuceHeader.h>

class TSLookAndFeel : public juce::LookAndFeel_V4
{
public:
    TSLookAndFeel();
    ~TSLookAndFeel() override;

    void drawRotarySlider(juce::Graphics&,
        int x, int y, int width, int height,
        float sliderPosProportional,
        float rotaryStartAngle,
        float rotaryEndAngle,
        juce::Slider&) override;
};