/*
  ==============================================================================

    TSLnF.cpp
    Created: 10 Oct 2021 5:15:30pm
    Author:  ihorv

  ==============================================================================
*/

#include "TSLnF.h"

TSLookAndFeel::TSLookAndFeel()
{
}

TSLookAndFeel::~TSLookAndFeel()
{
}

void TSLookAndFeel::drawRotarySlider(juce::Graphics& g, int x, int y, int width, int height, float sliderPosProportional, float rotaryStartAngle, float rotaryEndAngle, juce::Slider& slider)
{
    using namespace juce;
    slider.setPaintingIsUnclipped(true);

    auto bounds = Rectangle<float>(x, y, width, height * 0.7);

    g.setColour(juce::Colours::black);
    Path outline;
    float size = bounds.getHeight();
    outline.addEllipse(bounds.getCentreX() - size / 2.f, bounds.getY(), size, size);
    g.fillPath(outline);


    auto center = bounds.getCentre();
    auto radius = 25.f;

    Path p;

    Rectangle<float> r;


    // Button
    r.setLeft(center.getX() - 1.7);
    r.setRight(center.getX() + 1.7);
    r.setTop(bounds.getY());
    r.setBottom(center.getY());

    p.addRectangle(r);
    p.addEllipse(center.getX() - radius / 2.f, center.getY() - radius / 2.f, radius, radius);

    jassert(rotaryStartAngle <= rotaryEndAngle);

    auto sliderAngleInRadians = rotaryStartAngle + sliderPosProportional * (rotaryEndAngle - rotaryStartAngle);

    p.applyTransform(AffineTransform().rotated(sliderAngleInRadians, center.getX(), center.getY()));

    g.setColour(Colours::darkgrey);
    g.fillPath(p);

    g.setColour(Colours::antiquewhite);
    for (int i = 0; i < 7; ++i)
    {

        Path indicator;
        Rectangle<float> indRect;


        indRect.setLeft(center.getX() - 1.2f);
        indRect.setRight(center.getX() + 1.2f);
        indRect.setTop(center.getY() - size / 2.f - 6.f);
        indRect.setBottom(center.getY() - size / 2.f);
        indicator.addRectangle(indRect);
        float radians = rotaryStartAngle + ((float)i / 6.f) * (rotaryEndAngle - rotaryStartAngle);
        indicator.applyTransform(AffineTransform().rotated(radians, center.getX(), center.getY()));
        g.fillPath(indicator);
    }
}
