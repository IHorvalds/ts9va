/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

//==============================================================================
/**
*/

using Oversampler = juce::dsp::Oversampling<float>;
using Filter = juce::dsp::IIR::Filter<float>;
using Coefficients = Filter::CoefficientsPtr;
using Gain = juce::dsp::Gain<float>;

struct Settings
{
    float input{ 0 }, output{ 0 };
    float drive{ 0 }, tone{ 0 }, volume{ 0 };
    bool bypass { false };
};


class TubeScreamerAudioProcessor  : public juce::AudioProcessor
{
public:
    //==============================================================================
    TubeScreamerAudioProcessor();
    ~TubeScreamerAudioProcessor() override;

    //==============================================================================
    void prepareToPlay (double sampleRate, int samplesPerBlock) override;
    void releaseResources() override;

   #ifndef JucePlugin_PreferredChannelConfigurations
    bool isBusesLayoutSupported (const BusesLayout& layouts) const override;
   #endif

    void processBlock (juce::AudioBuffer<float>&, juce::MidiBuffer&) override;

    //==============================================================================
    juce::AudioProcessorEditor* createEditor() override;
    bool hasEditor() const override;

    //==============================================================================
    const juce::String getName() const override;

    bool acceptsMidi() const override;
    bool producesMidi() const override;
    bool isMidiEffect() const override;
    double getTailLengthSeconds() const override;

    //==============================================================================
    int getNumPrograms() override;
    int getCurrentProgram() override;
    void setCurrentProgram (int index) override;
    const juce::String getProgramName (int index) override;
    void changeProgramName (int index, const juce::String& newName) override;

    //==============================================================================
    void getStateInformation (juce::MemoryBlock& destData) override;
    void setStateInformation (const void* data, int sizeInBytes) override;

    Settings getSettings();

    static juce::AudioProcessorValueTreeState::ParameterLayout CreateParameterLayout();
    juce::AudioProcessorValueTreeState m_apvts{ *this, nullptr, "Parameters", CreateParameterLayout() };

    // Tone Knob Stage
    float toneStageSimulation(float Vi, float alpha, float beta); // P1 = 20k * alpha. P2 = 20k * (1-alpha). alpha tone, beta volume
    
    // Clipping Stage
    float clipStageSimulation(float Vi, float alpha);

private:

    Oversampler os{ 2, 2, Oversampler::FilterType::filterHalfBandPolyphaseIIR };
    Gain input_level, output_level;
    double m_sampleRate = 44100.f;

    // Start Tone Stage Variables
    float tone_stage_current_x1 = 0, 
          tone_stage_current_x2 = 0,
          tone_stage_current_x3 = 0,
          tone_stage_current_x4 = 0;
    // End Tone Stage Variables

    // Start Clipping Stage Variables
    
    float clip_stage_current_x1 = 0,
          clip_stage_current_x2 = 0;
    float clip_stage_vd = 0;
    const float c_R = 51e3, c_Pot = 500e3;

    // End Clipping Stage Variables

    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (TubeScreamerAudioProcessor)
};
