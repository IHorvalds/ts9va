/*
  ==============================================================================

    BypassLEDButton.h
    Created: 11 Oct 2021 10:41:52am
    Author:  ihorv

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

class BypassLEDButton : public juce::ToggleButton
{

public:

    BypassLEDButton(juce::Colour onColour, juce::Colour offColour);
    BypassLEDButton();
    ~BypassLEDButton();

    void paintButton(juce::Graphics& g, bool shouldDrawButtonAsHighlighted, bool shouldDrawButtonAsDown) override;

private:

    juce::Colour m_onColour = juce::Colours::orangered, m_offColour = juce::Colours::lightgrey;
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (BypassLEDButton)
};