/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"
#include <limits>

//==============================================================================
TubeScreamerAudioProcessor::TubeScreamerAudioProcessor()
#ifndef JucePlugin_PreferredChannelConfigurations
     : AudioProcessor (BusesProperties()
                     #if ! JucePlugin_IsMidiEffect
                      #if ! JucePlugin_IsSynth
                       .withInput  ("Input",  juce::AudioChannelSet::stereo(), true)
                      #endif
                       .withOutput ("Output", juce::AudioChannelSet::stereo(), true)
                     #endif
                       )
#endif
{
}

TubeScreamerAudioProcessor::~TubeScreamerAudioProcessor()
{
}

//==============================================================================
const juce::String TubeScreamerAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

bool TubeScreamerAudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool TubeScreamerAudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

bool TubeScreamerAudioProcessor::isMidiEffect() const
{
   #if JucePlugin_IsMidiEffect
    return true;
   #else
    return false;
   #endif
}

double TubeScreamerAudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int TubeScreamerAudioProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int TubeScreamerAudioProcessor::getCurrentProgram()
{
    return 0;
}

void TubeScreamerAudioProcessor::setCurrentProgram (int index)
{
}

const juce::String TubeScreamerAudioProcessor::getProgramName (int index)
{
    return {};
}

void TubeScreamerAudioProcessor::changeProgramName (int index, const juce::String& newName)
{
}

//==============================================================================
void TubeScreamerAudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
    os.reset();
    os.initProcessing(samplesPerBlock);
    m_sampleRate = os.getOversamplingFactor() * sampleRate;
}

void TubeScreamerAudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool TubeScreamerAudioProcessor::isBusesLayoutSupported (const BusesLayout& layouts) const
{
  #if JucePlugin_IsMidiEffect
    juce::ignoreUnused (layouts);
    return true;
  #else
    // This is the place where you check if the layout is supported.
    // In this template code we only support mono or stereo.
    // Some plugin hosts, such as certain GarageBand versions, will only
    // load plugins that support stereo bus layouts.
    if (layouts.getMainOutputChannelSet() != juce::AudioChannelSet::mono()
     && layouts.getMainOutputChannelSet() != juce::AudioChannelSet::stereo())
        return false;

    // This checks if the input layout matches the output layout
   #if ! JucePlugin_IsSynth
    if (layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
        return false;
   #endif

    return true;
  #endif
}
#endif

float TubeScreamerAudioProcessor::toneStageSimulation(float Vi, float alpha, float beta)
{
    const float Fs = (float)m_sampleRate;
    const float Ts = 1 / Fs;
    const float t_C1 = .22e-6; const float t_R1 = Ts / (2 * t_C1);
    const float t_C2 = .22e-6; const float t_R2 = Ts / (2 * t_C2);
    const float t_C3 = .22e-6; const float t_R3 = Ts / (2 * t_C3);
    const float t_C4 = 1e-6;   const float t_R4 = Ts / (2 * t_C4);

    const float t_R5 = 220, t_R6 = 1000, t_R7 = 1000, t_R8 = 220, t_R9 = 1000, t_P_TONE = 20000, t_P_VOL = 100000;

    const float R10 = t_P_VOL * (1 - beta), R11 = t_P_VOL * beta;
    const float P1 = t_P_TONE * alpha, P2 = t_P_TONE * (1 - alpha);

    const float G2 = 1 + t_R2 / P1 + t_R5 / P1;
    const float G3 = 1 + t_R3 / P2 + t_R8 / P2;
    const float Go = 1 + R10 / R11 + t_R9 / R11 + t_R4 / R11;
    const float Gx = 1 + t_R7 / (G3 * P2);
    const float Gz = 1 / t_R1 + 1 / (G2 * P1) + 1 / t_R6;
    const float Gr = P1 / t_R2 + 1 + t_R5 / t_R2;
    const float Gs = 1 + P2 / t_R3 + t_R8 / t_R3;

    float b0 = Gx / (Go * t_R6 * Gz);
    float b1 = Gx / (Go * Gz);
    float b2 = Gx * t_R2 / (G2 * Gz * Go * P1);
    float b3 = -t_R3 * t_R7 / (Go * G3 * P2);
    float b4 = -t_R4 / Go;

    float Vo = b0 * Vi + b1 * this->tone_stage_current_x1 + b2 * this->tone_stage_current_x2 + b3 * this->tone_stage_current_x3 + b4 * this->tone_stage_current_x4;

    // update state
    float Vx = Vi / (t_R6 * Gz) + this->tone_stage_current_x1 / Gz + (t_R2 * this->tone_stage_current_x2) / (G2 * Gz * P1);
    this->tone_stage_current_x1 = 2 / t_R1 * Vx - this->tone_stage_current_x1;
    this->tone_stage_current_x2 = 2 / t_R2 * (Vx/Gr + ((P1 + t_R5)/Gr) * this->tone_stage_current_x2) - this->tone_stage_current_x2;
    this->tone_stage_current_x3 = 2 / t_R3 * (Vx/Gs + ((P2 + t_R8)/Gs) * this->tone_stage_current_x3) - this->tone_stage_current_x3;
    this->tone_stage_current_x4 = 2 * Vo / R11 + this->tone_stage_current_x4;
    return Vo;
}



inline float clipFunction(const float& Vd, const float& Vi, const float& G4, const float& R1, const float& R4, const float& x1, const float& x2, const float& R2, const float& R3, const float& Is, const float& niu, const float& Vt)
{
    return -Vi / (G4 * R4) + R1 / (G4 * R4) * x1 - x2 + Vd / R2 + Vd / R3 + 2.f * Is * sinh(Vd / (niu * Vt));
}

inline float clipFunctionDerived(const float& Vd, const float& R2, const float& R3, const float& Is, const float& niu, const float& Vt)
{
    return 1.f / R2 + 1.f / R3 + (2.f * Is) / (niu * Vt) * cosh(Vd / (niu * Vt));
}

float TubeScreamerAudioProcessor::clipStageSimulation(float Vi, float alpha)
{
    const float Fs = (float)m_sampleRate;
    const float Ts = 1 / Fs;
    const float C1 = 47e-9, R1 = Ts / (2 * C1);
    const float C2 = 51e-12, R2 = Ts / (2 * C2);
    const float R4 = 4.7e3;
    float R3 = c_R + c_Pot * alpha;

    float G1 = 1 + R4 / R1;
    float G4 = 1 + R1 / R4;

    const float Is = 1e-15;
    const float niu = 1.f;
    const float Vt = 26e-3;

    float epsilon = 0.00000000001f;
    float x2 = this->clip_stage_vd;
    size_t iter = 0;
    float b = 1.f;
    float fx1 = clipFunction(this->clip_stage_vd, Vi, G4, R1, R4, this->clip_stage_current_x1, this->clip_stage_current_x2, R2, R3, Is, niu, Vt);
    float fdx1 = clipFunctionDerived(this->clip_stage_vd, R2, R3, Is, niu, Vt);

    while (iter < 15 && abs(fx1) > epsilon)
    {
        fdx1 = clipFunctionDerived(this->clip_stage_vd, R2, R3, Is, niu, Vt);
        x2 = this->clip_stage_vd - b * fx1 / fdx1;
        float fx2 = clipFunction(x2, Vi, G4, R1, R4, this->clip_stage_current_x1, this->clip_stage_current_x2, R2, R3, Is, niu, Vt);
        if (abs(fx2) < abs(fx1))
        {
            this->clip_stage_vd = x2;
            b = 1.f;
        }
        else
        {
            b *= 0.5;
        }

        fx1 = clipFunction(this->clip_stage_vd, Vi, G4, R1, R4, this->clip_stage_current_x1, this->clip_stage_current_x2, R2, R3, Is, niu, Vt);
        iter++;
    }

    float Vo = Vi + this->clip_stage_vd;

    this->clip_stage_current_x1 = (2.f / R1) * (Vi / G1 + this->clip_stage_current_x1 * R4 / G1) - this->clip_stage_current_x1;
    this->clip_stage_current_x2 = (2.f / R2) * (this->clip_stage_vd)- this->clip_stage_current_x2;
    return Vo;
}

void TubeScreamerAudioProcessor::processBlock (juce::AudioBuffer<float>& buffer, juce::MidiBuffer& midiMessages)
{
    auto totalNumInputChannels  = getTotalNumInputChannels();
    auto totalNumOutputChannels = getTotalNumOutputChannels();
    
    auto settings = getSettings();

    for (auto i = totalNumInputChannels; i < totalNumOutputChannels; ++i)
        buffer.clear (i, 0, buffer.getNumSamples());

    input_level.setGainDecibels(settings.input);
    output_level.setGainDecibels(settings.output);

    juce::dsp::AudioBlock<float> block(buffer);
    juce::dsp::AudioBlock<float> osBlock;

    setLatencySamples(juce::roundToInt(os.getLatencyInSamples()));
    osBlock = os.processSamplesUp(block);

    auto numSamples = osBlock.getNumSamples();
    auto numChannels = buffer.getNumChannels();

    auto* input = osBlock.getChannelPointer(0); // get mono input

    for (int n = 0; n < numSamples; ++n) {
        float y = input_level.processSample(input[n]);
        if (!settings.bypass) 
        {
            y = clipStageSimulation(y, settings.drive);
            y = toneStageSimulation(y, settings.tone, settings.volume);
        }
        y = output_level.processSample(y);
        for (int channel = 0; channel < totalNumInputChannels; ++channel)
        {
            auto* output = osBlock.getChannelPointer(channel);
            output[n] = y;
        }
    }

    // downsample
    os.processSamplesDown(block);
}

//==============================================================================
bool TubeScreamerAudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

juce::AudioProcessorEditor* TubeScreamerAudioProcessor::createEditor()
{
    return new TubeScreamerAudioProcessorEditor(*this);
    //return new juce::GenericAudioProcessorEditor (*this);
}

//==============================================================================
void TubeScreamerAudioProcessor::getStateInformation (juce::MemoryBlock& destData)
{
    // You should use this method to store your parameters in the memory block.
    // You could do that either as raw data, or use the XML or ValueTree classes
    // as intermediaries to make it easy to save and load complex data.
    juce::MemoryOutputStream mos(destData, true);
    m_apvts.state.writeToStream(mos);
}

void TubeScreamerAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    auto tree = juce::ValueTree::readFromData(data, sizeInBytes);

    if (tree.isValid()) {
        m_apvts.replaceState(tree);

        Settings settings = getSettings();
    }
}

Settings TubeScreamerAudioProcessor::getSettings()
{
    Settings settings;

    // Bypass Button
    settings.bypass = m_apvts.getRawParameterValue(param_names[Parameters::k_bypass])->load();

    // Input Volume
    settings.input = m_apvts.getRawParameterValue(param_names[Parameters::k_input])->load();

    // Drive
    settings.drive = m_apvts.getRawParameterValue(param_names[Parameters::k_drive])->load();

    // Tone
    settings.tone = m_apvts.getRawParameterValue(param_names[Parameters::k_tone])->load();

    // Volume
    settings.volume = m_apvts.getRawParameterValue(param_names[Parameters::k_volume])->load();

    // Output level
    settings.output = m_apvts.getRawParameterValue(param_names[Parameters::k_output])->load();

    return settings;
}

juce::AudioProcessorValueTreeState::ParameterLayout TubeScreamerAudioProcessor::CreateParameterLayout()
{
    juce::AudioProcessorValueTreeState::ParameterLayout layout;

    // Bypass Button
    layout.add(std::make_unique<juce::AudioParameterBool>(param_names[Parameters::k_bypass],
                                                          param_names[Parameters::k_bypass],
                                                          false));

    // input
    layout.add(std::make_unique<juce::AudioParameterFloat>(param_names[Parameters::k_input],
                                                            param_names[Parameters::k_input],
                                                            juce::NormalisableRange<float>(-50.f, 10.f, .1f, 3.75f),
                                                            0.f));

    // We divide by values proportional to these later. They mustn't reach either 0 or 1.
    // Drive
    layout.add(std::make_unique<juce::AudioParameterFloat>(param_names[Parameters::k_drive],
                                                            param_names[Parameters::k_drive],
                                                            juce::NormalisableRange<float>(0.0005f, .998f),
                                                            .5f));

    // Tone
    layout.add(std::make_unique<juce::AudioParameterFloat>(param_names[Parameters::k_tone],
                                                            param_names[Parameters::k_tone],
                                                            juce::NormalisableRange<float>(.0014f, .99f),
                                                            .5f));

    // Volume
    layout.add(std::make_unique<juce::AudioParameterFloat>(param_names[Parameters::k_volume],
                                                            param_names[Parameters::k_volume],
                                                            juce::NormalisableRange<float>(.002f, .9951f),
                                                            .5f));

    // Output Volume
    layout.add(std::make_unique<juce::AudioParameterFloat>(param_names[Parameters::k_output],
                                                            param_names[Parameters::k_output],
                                                            juce::NormalisableRange<float>(-50.f, 10.f, 1.f, 3.75f),
                                                            0.f));

    return layout;
}


//==============================================================================
// This creates new instances of the plugin..
juce::AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new TubeScreamerAudioProcessor();
}
