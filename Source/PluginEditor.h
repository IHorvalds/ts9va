/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "PluginProcessor.h"
#include "RotarySlider.h"
#include "BypassLEDButton.h"
#include "BypassButton.h"
#include "Parameters.h"
#include "SimpleRotarySlider.h"

//==============================================================================
/**
*/
class TubeScreamerAudioProcessorEditor  : public juce::AudioProcessorEditor
{
public:
    TubeScreamerAudioProcessorEditor (TubeScreamerAudioProcessor&);
    ~TubeScreamerAudioProcessorEditor() override;

    //==============================================================================
    void paint (juce::Graphics&) override;
    void resized() override;

private:
    // This reference is provided as a quick way for your editor to
    // access the processor object that created it.
    TubeScreamerAudioProcessor& audioProcessor;

    using APVTS = juce::AudioProcessorValueTreeState;
    using SliderAttachment = APVTS::SliderAttachment;
    using ButtonAttachment = APVTS::ButtonAttachment;

    RotarySlider driveSlider, toneSlider, volumeSlider;
    SimpleRotarySlider inputSlider, outputSlider;
    BypassLEDButton bypassLED;
    BypassButton bypassButton;
    SliderAttachment inputAttachment, driveAttachment, toneAttachment, volAttachment, outputAttachment;
    ButtonAttachment bypassLEDAttachment, bypassButtonAttachment;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (TubeScreamerAudioProcessorEditor)
};
