/*
  ==============================================================================

    TubeScreamerComponent.cpp
    Created: 10 Oct 2021 4:54:39pm
    Author:  ihorv

  ==============================================================================
*/

#include <JuceHeader.h>
#include "RotarySlider.h"

//==============================================================================
RotarySlider::RotarySlider() :
    juce::Slider(juce::Slider::SliderStyle::RotaryHorizontalVerticalDrag, juce::Slider::TextEntryBoxPosition::TextBoxBelow)
{
    // In your constructor, you should add any child components, and
    // initialise any special settings that your component needs.
    m_param = nullptr;
    setLookAndFeel(&m_lookAndFeel);
}

RotarySlider::RotarySlider(juce::RangedAudioParameter* rap) : m_param(rap), juce::Slider(juce::Slider::SliderStyle::RotaryHorizontalVerticalDrag, juce::Slider::TextEntryBoxPosition::TextBoxBelow)
{
    setLookAndFeel(&m_lookAndFeel);
}

RotarySlider::~RotarySlider()
{
    setLookAndFeel(nullptr);
}

void RotarySlider::paint (juce::Graphics& g)
{
    using namespace juce;

    setPaintingIsUnclipped(true);

    auto startAngle = degreesToRadians(180.f + 45.f);
    auto endAngle = MathConstants<float>::twoPi + degreesToRadians(180.f - 45.f);

    auto range = getRange();


    auto sliderBounds = getSliderBounds();

    getLookAndFeel().drawRotarySlider(g,
                                    sliderBounds.getX(), sliderBounds.getY(),
                                    sliderBounds.getWidth(),
                                    sliderBounds.getHeight(),
                                    valueToProportionOfLength(getValue()),
                                    startAngle,
                                    endAngle,
                                    *this);


    Font font(getTextHeight(), Font::FontStyleFlags::bold);
    g.setFont(font);
    auto strWidth = g.getCurrentFont().getStringWidth(getDisplayString());

    auto bounds = getLocalBounds();
    Rectangle<float> labelRect;

    auto height = getTextHeight() + 4;
    labelRect.setSize(strWidth + 4, height);
    labelRect.setCentre(bounds.getCentreX(), bounds.getBottom() - height);

    g.setColour(juce::Colours::white);
    g.drawFittedText(getDisplayString(), labelRect.toNearestInt(), Justification::centred, 1);
}

juce::String RotarySlider::getDisplayString() const
{
    return m_param->paramID;
}

void RotarySlider::resized()
{
    // This method is where you should set the bounds of any child
    // components that your component contains..

}

juce::Rectangle<int> RotarySlider::getSliderBounds() const
{
    auto bounds = getLocalBounds();
    auto size = juce::jmin(bounds.getWidth(), bounds.getHeight());

    juce::Rectangle<int> r;
    r.setSize(size, size);
    r.setCentre(bounds.getCentre());
    //r.setY(bounds.getY());

    return r;
}