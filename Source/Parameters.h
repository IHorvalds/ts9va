/*
  ==============================================================================

    Constants.h
    Created: 9 Oct 2021 10:18:35am
    Author:  ihorv

  ==============================================================================
*/

#pragma once

#ifndef PARAMS
#define PARAMS 1

inline const char *param_names[] = {"Input", "Drive", "Tone", "Volume", "Output", "Bypass"};

class Parameters {
private:
    enum param_name {
        input,
        drive,
        tone,
        volume,
        output,
        bypass
    };
public:
    static const param_name k_drive = drive;
    static const param_name k_tone = tone;
    static const param_name k_volume = volume;
    static const param_name k_input = input;
    static const param_name k_output = output;
    static const param_name k_bypass = bypass;
};
#endif