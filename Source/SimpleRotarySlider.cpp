/*
  ==============================================================================

    SimpleRotarySlider.cpp
    Created: 11 Oct 2021 10:08:24pm
    Author:  ihorv

  ==============================================================================
*/

#include <JuceHeader.h>
#include "SimpleRotarySlider.h"

//==============================================================================
SimpleRotarySlider::SimpleRotarySlider() : juce::Slider(juce::Slider::SliderStyle::RotaryHorizontalVerticalDrag, juce::Slider::TextEntryBoxPosition::TextBoxAbove)
{
    // In your constructor, you should add any child components, and
    // initialise any special settings that your component needs.
    m_param = nullptr;
}

SimpleRotarySlider::SimpleRotarySlider(juce::RangedAudioParameter* rap) : juce::Slider(juce::Slider::SliderStyle::RotaryHorizontalVerticalDrag, juce::Slider::TextEntryBoxPosition::TextBoxAbove), m_param(rap)
{
}

SimpleRotarySlider::~SimpleRotarySlider()
{
}

void SimpleRotarySlider::paint (juce::Graphics& g)
{
    using namespace juce;

    setPaintingIsUnclipped(true);

    auto startAngle = degreesToRadians(180.f + 45.f);
    auto endAngle = MathConstants<float>::twoPi + degreesToRadians(180.f - 45.f);

    auto range = getRange();


    auto sliderBounds = getSliderBounds();
    auto fullBounds = getLocalBounds();

    g.setColour(juce::Colours::darkgrey);
    Path outline;
    float size = sliderBounds.getHeight() / 1.2f;
    outline.addEllipse(sliderBounds.getCentreX() - size / 2.f, fullBounds.getCentreY() - size / 2.f, size, size);
    g.fillPath(outline);


    Path thumb;
    Rectangle<float> r;
    auto center = fullBounds.getCentre();

    // Button
    r.setLeft(center.getX() - 1.7);
    r.setRight(center.getX() + 1.7);
    r.setTop(center.getY() - size / 2.f);
    r.setBottom(center.getY() - 3.f);

    thumb.addRectangle(r);

    auto sliderPosProportional = valueToProportionOfLength(getValue());
    auto sliderAngleInRadians = startAngle + sliderPosProportional * (endAngle - startAngle);

    thumb.applyTransform(AffineTransform().rotated(sliderAngleInRadians, center.getX(), center.getY()));

    g.setColour(Colours::black);
    g.fillPath(thumb);

    g.setColour(Colours::antiquewhite);
    // indicators
    for (int i = 0; i < 7; ++i)
    {

        Path indicator;
        Rectangle<float> indRect;


        indRect.setLeft(center.getX() - 0.8f);
        indRect.setRight(center.getX() + 0.8f);
        indRect.setTop(center.getY() - size / 2.f - 4.f);
        indRect.setBottom(center.getY() - size / 2.f);
        indicator.addRectangle(indRect);
        float radians = startAngle + ((float)i / 6.f) * (endAngle - startAngle);
        indicator.applyTransform(AffineTransform().rotated(radians, center.getX(), center.getY()));
        g.fillPath(indicator);
    }

    Font font(10.f, Font::FontStyleFlags::bold);
    g.setFont(font);
    auto strWidth = g.getCurrentFont().getStringWidth(getDisplayString());

    auto bounds = getLocalBounds();
    Rectangle<float> labelRect;

    auto height = 10.f;
    labelRect.setSize(strWidth, height);
    labelRect.setCentre(center.getX(), center.getY() + size);

    g.setColour(juce::Colours::white);
    g.drawFittedText(getDisplayString(), labelRect.toNearestInt(), Justification::centred, 1);

    // 0 dB label
    Font labelFont(10.f, Font::FontStyleFlags::bold);
    g.setFont(labelFont);
    strWidth = g.getCurrentFont().getStringWidth("0 dB");

    labelRect.setSize(strWidth, height);
    labelRect.setCentre(center.getX(), center.getY() - size);
    g.drawFittedText("0 dB", labelRect.toNearestInt(), Justification::centred, 1);
}

void SimpleRotarySlider::resized()
{
    // This method is where you should set the bounds of any child
    // components that your component contains..

}

juce::Rectangle<int> SimpleRotarySlider::getSliderBounds() const
{
    auto bounds = getLocalBounds();
    auto size = juce::jmin(bounds.getWidth(), bounds.getHeight()) * 0.8f;

    juce::Rectangle<int> r;
    r.setSize(size, size);
    r.setCentre(bounds.getCentre());

    return r;
}

juce::String SimpleRotarySlider::getDisplayString() const
{
    return m_param->paramID;
}