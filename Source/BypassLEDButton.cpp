/*
  ==============================================================================

    BypassLEDButton.cpp
    Created: 11 Oct 2021 10:41:52am
    Author:  ihorv

  ==============================================================================
*/

#include "BypassLEDButton.h"

BypassLEDButton::BypassLEDButton(juce::Colour onColour, juce::Colour offColour) :
    juce::ToggleButton(),
    m_onColour(onColour), m_offColour(offColour)
{
}

BypassLEDButton::BypassLEDButton() : juce::ToggleButton()
{
}

BypassLEDButton::~BypassLEDButton()
{
}

void BypassLEDButton::paintButton(juce::Graphics& g, bool shouldDrawButtonAsHighlighted, bool shouldDrawButtonAsDown)
{
    using namespace juce;

    setPaintingIsUnclipped(true);

    auto bounds = this->getLocalBounds().toFloat();


    Path p;
    float size = jmin(bounds.getWidth(), bounds.getHeight());
    p.addEllipse(bounds.getCentreX() - size / 2.f, bounds.getCentreY() - size / 2.f, size, size);

    g.setColour(!this->getToggleState() ? this->m_onColour : this->m_offColour);
    g.fillPath(p);

}