/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
TubeScreamerAudioProcessorEditor::TubeScreamerAudioProcessorEditor (TubeScreamerAudioProcessor& p)
    : AudioProcessorEditor (&p), audioProcessor (p),
        inputSlider(audioProcessor.m_apvts.getParameter(param_names[Parameters::k_input])),
        driveSlider(audioProcessor.m_apvts.getParameter(param_names[Parameters::k_drive])),
        toneSlider(audioProcessor.m_apvts.getParameter(param_names[Parameters::k_tone])),
        volumeSlider(audioProcessor.m_apvts.getParameter(param_names[Parameters::k_volume])),
        outputSlider(audioProcessor.m_apvts.getParameter(param_names[Parameters::k_output])),
        bypassLED(),
        bypassButton(juce::Colours::darkgrey, juce::Colours::darkgrey),

        inputAttachment(audioProcessor.m_apvts, param_names[Parameters::k_input], inputSlider),
        driveAttachment(audioProcessor.m_apvts, param_names[Parameters::k_drive], driveSlider),
        toneAttachment(audioProcessor.m_apvts, param_names[Parameters::k_tone], toneSlider),
        volAttachment(audioProcessor.m_apvts, param_names[Parameters::k_volume], volumeSlider),
        outputAttachment(audioProcessor.m_apvts, param_names[Parameters::k_output], outputSlider),
        bypassLEDAttachment(audioProcessor.m_apvts, param_names[Parameters::k_bypass], bypassLED),
        bypassButtonAttachment(audioProcessor.m_apvts, param_names[Parameters::k_bypass], bypassButton)
{
    // Make sure that before the constructor has finished, you've set the
    // editor's size to whatever you need it to be.
    addAndMakeVisible(inputSlider);
    addAndMakeVisible(driveSlider);
    addAndMakeVisible(toneSlider);
    addAndMakeVisible(volumeSlider);
    addAndMakeVisible(outputSlider);
    addAndMakeVisible(bypassLED);
    addAndMakeVisible(bypassButton);
    setSize (400, 300);
}

TubeScreamerAudioProcessorEditor::~TubeScreamerAudioProcessorEditor()
{
}

//==============================================================================
void TubeScreamerAudioProcessorEditor::paint (juce::Graphics& g)
{
    
    setPaintingIsUnclipped(true);

    g.fillAll (juce::Colours::black);
    auto bounds = getLocalBounds();

    juce::Rectangle<float> rect;
    rect.setSize(150.f, 270.f);
    rect.setCentre(juce::Point<float>(bounds.getCentreX(), bounds.getCentreY()));
    g.setColour(juce::Colours::darkgreen);
    g.fillRoundedRectangle(rect, 10.f);

    juce::Path p;
    rect.removeFromBottom(20.f);
    auto bypassButtonArea = rect.removeFromBottom(50.f);
    juce::Point<float> bypassCentre = bypassButtonArea.getCentre();

    bypassButtonArea.setSize(50.f, 50.f);
    bypassButtonArea.setCentre(bypassCentre);
    p.addEllipse(bypassButtonArea);

    g.setColour(juce::Colours::antiquewhite);
    g.fillPath(p);
}

void TubeScreamerAudioProcessorEditor::resized()
{
    auto bounds = getLocalBounds();

    juce::Rectangle<int> rect;
    rect.setSize(150, 270);
    rect.setCentre(bounds.getCentre());
    
    rect.removeFromTop(20);
    float h = rect.getHeight() / 3.f;
    auto driveVolumeBounds = rect.removeFromTop(h);
    auto driveBounds = driveVolumeBounds.removeFromLeft(driveVolumeBounds.getWidth() * 5.f / 11.f);
    
    // Drive Knob
    driveSlider.setBounds(driveBounds);

    // Bypass LED
    auto bypassLEDBounds = driveVolumeBounds.removeFromLeft(driveVolumeBounds.getWidth() / 6.f);

    bypassLED.setBounds(bypassLEDBounds);
    
    // Volume knob
    volumeSlider.setBounds(driveVolumeBounds);

    // Tone Knob, Input Knob, Output Knob
    auto toneBounds = rect.removeFromTop(h);
    /*toneBounds.removeFromLeft(10.f);
    toneBounds.removeFromRight(10.f);*/
    auto toneCenter = toneBounds.getCentre();

    auto outputBounds = toneBounds.removeFromLeft(toneBounds.getWidth() / 5.f);
    juce::Point<int> c_output = outputBounds.getCentre();
    c_output.addXY(10, 0);
    outputBounds.setCentre(c_output);
    outputSlider.setBounds(outputBounds);

    auto inputBounds = toneBounds.removeFromRight(toneBounds.getWidth() / 4.f);
    juce::Point<int> c_input = inputBounds.getCentre();
    c_input.addXY(-10, 0);
    inputBounds.setCentre(c_input);
    inputSlider.setBounds(inputBounds);
    
    toneBounds.setSize(driveBounds.getWidth(), toneBounds.getHeight());
    toneBounds.setCentre(toneCenter);
    toneSlider.setBounds(toneBounds);


    rect.removeFromBottom(20.f);
    auto bypassButtonArea = rect.removeFromBottom(50.f);
    juce::Point<int> bypassCentre = bypassButtonArea.getCentre();

    bypassButtonArea.setSize(30, 30);
    bypassButtonArea.setCentre(bypassCentre);
    bypassButton.setBounds(bypassButtonArea);
}
