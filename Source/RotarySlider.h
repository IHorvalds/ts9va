/*
  ==============================================================================

    TubeScreamerComponent.h
    Created: 10 Oct 2021 4:54:39pm
    Author:  ihorv

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "TSLnF.h"

//==============================================================================
/*
*/
class RotarySlider  : public juce::Slider
{
public:
    RotarySlider();
    RotarySlider(juce::RangedAudioParameter* rap);
    ~RotarySlider() override;

    void paint (juce::Graphics&) override;
    void resized() override;
    juce::Rectangle<int> getSliderBounds() const;
    juce::String getDisplayString() const;
    int getTextHeight() const { return 14; }

private:
    juce::RangedAudioParameter* m_param;

    TSLookAndFeel m_lookAndFeel;
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RotarySlider)
};
